package ar.edu.unju.fi.demotp7.model;

import org.springframework.stereotype.Component;

@Component
public class Compras {
	private int idCompras;
	private Producto Producto;
	private int canditdad;
	public Compras() {
		// TODO Auto-generated constructor stub
	}
	public Compras(int idCompras, ar.edu.unju.fi.demotp7.model.Producto producto, int canditdad) {
		super();
		this.idCompras = idCompras;
		Producto = producto;
		this.canditdad = canditdad;
	}
	public int getIdCompras() {
		return idCompras;
	}
	public void setIdCompras(int idCompras) {
		this.idCompras = idCompras;
	}
	public Producto getProducto() {
		return Producto;
	}
	public void setProducto(Producto producto) {
		Producto = producto;
	}
	public int getCanditdad() {
		return canditdad;
	}
	public void setCanditdad(int canditdad) {
		this.canditdad = canditdad;
	}
	
}

