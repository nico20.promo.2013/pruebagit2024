package ar.edu.unju.fi.demotp7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demotp7Application {

	public static void main(String[] args) {
		SpringApplication.run(Demotp7Application.class, args);
	}

}
